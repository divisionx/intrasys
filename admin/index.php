<?php
session_start();
ob_start();
include_once "../inc/config.php";
include_once "../inc/funciones.php";
//include "inc/cabecera.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="estiloadmin.css" />
</head>
<body>
<div id=contenido>
<?php
if ($_SESSION['niveluser']==1)
{
  
?>
<div class="menu">
<!-- <div id="menu_i"></div>    <div id="menu_d"></div> -->
<ul>
  <li><a href="index.php">Partes</a>
    <ul>
      <li><a href="index.php?op=borrarinc">Borrar</a></li>
    </ul>
  </li>
  <li><a href="index.php">Usuarios</a>
    <ul>
      <li><a href="index.php?op=altauser">Crear</a></li>
      <li><a href="index.php?op=promo">Promocionar</a></li>
      <li><a href="index.php?op=borrauser">Borrar</a></li>
    </ul>
  </li>
  <li><a href="../index.php">Salir</a></li>
</ul>

</div><!-- fin Menu -->
<?php
}
?>

<h1>De gustibus et coloribus non disputandum</h1>

<?php
if (! isset($_SESSION['sesion'])) //verificamos si NO existe una sesion abierta
{
  switch ($_REQUEST['op'])
  {
    default: //mostramos un menu, registro o login
      echo "<p class=aviso>No tienes permiso para estar aqui</p>";
    break;
  }
}
else
{
  if ($_SESSION['niveluser']==1)
  {
    switch ($_REQUEST['op'])
    {
      default:
	echo "Bienvenido al panel de administracion <br/>";
	echo "<p class=aviso>Recuerda que todo lo que hagas aqui tendra repercusiones en el funcionamiento del programa. Algunos cambios que realices desde aqui, como la eliminacion de partes o usuarios NO tienen vuelta atras.<br/>Recuerda, un gran poder conlleva una gran responsabilidad.</p>";
      break;
      case "promo": //promocionar un usuario a tecnico
	include "promocionar.php";
      break;
      case "borrarinc":
	include "borrainc.php";
      break;
      case "altauser":
	include "creauser.php";
      break;
      case "borrauser":
	include "borrauser.php";
      break;
    }
  }
  else
  {
    echo "<p class=aviso>No eres un administrador.</p>";
  }
}

?>
</div>
<div id=footer>
<p><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-sa/3.0/80x15.png" /></a></p>
</div>
</body>
</html>
 
