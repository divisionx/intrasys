<?php
ob_start();
define('FPDF_FONTPATH',"$_SERVER[DOCUMENT_ROOT]/inc/class/font");
include 'fpdf.php';
include '../config.php';

$pdf=new FPDF();
$pdf->AddPage();

$pdf->SetFont('Arial','B',16);
$pdf->Cell(50);
$pdf->Cell(90,10,"Parte de incidencias",1,1,'C');

// sacamos los datos por el id de incidencia
$id = $_GET['id'];
$sql='SELECT * FROM partes WHERE id=\''.$id.'\'';
$resultado = mysql_query($sql);
$row=mysql_fetch_array($resultado);

// resolvemos el nombre por el id
$sqlnombre='SELECT nombre FROM usuarios WHERE id=\''.$row['enviado_por'].'\'';
$nombre=mysql_query($sqlnombre);
$rownombre=mysql_fetch_array($nombre);


$pdf->SetFont('Arial','',14);

// Linea "Id de incidencia" 30
$pdf->SetXY(20,30);
$pdf->Cell(60,10,'Id de incidencia:',1,1);
$pdf->SetXY(80,30);
$pdf->Cell(100,10,"$row[id]",1,1);

// Linea "Persona de contacto" 40
$pdf->SetXY(20,40);
$pdf->Cell(60,10,'Persona de contacto:',1,1);
$pdf->SetXY(80,40);
$pdf->Cell(100,10,"$rownombre[nombre]",1,1);

// Linea "Fecha" 50
$pdf->SetXY(20,50);
$pdf->Cell(60,10,'Fecha:',1,1);
$pdf->SetXY(80,50);
$pdf->Cell(100,10,"$row[fecha]",1,1);

// Linea "Marca" 60
$pdf->SetXY(20,60);
$pdf->Cell(60,10,'Marca:',1,1);
$pdf->SetXY(80,60);
$pdf->Cell(100,10,"$row[marca]",1,1);

// Linea "Modelo" 70
$pdf->SetXY(20,70);
$pdf->Cell(60,10,'Modelo:',1,1);
$pdf->SetXY(80,70);
$pdf->Cell(100,10,"$row[modelo]",1,1);

// Linea "ubicacion" 80
$pdf->SetXY(20,80);
$pdf->Cell(60,10,'Ubicacion:',1,1);
$pdf->SetXY(80,80);
$pdf->Cell(100,10,"$row[localizacion]",1,1);

// Linea "incidencia" 90
$pdf->SetXY(20,90);
$pdf->Cell(160,10,'Incidencia:',1,1, 'C');
$pdf->SetXY(20,100);
//$pdf->Cell(160,100,"$row[incidencia]",1,1);
$pdf->MultiCell(160,10,"$row[incidencia]",1);

// Linea "ubicacion" 80
$pdf->SetXY(20,210);
$pdf->Cell(160,10,'Observaciones/Solucion:',1,1, 'C');
$pdf->SetXY(20,220);
$pdf->Cell(160,50,"",1,1);

$pdf->Output();
?>