<?php
include "config.php";
//esta funcion devuelve la fecha formateada DD/MM/AAAA

function fecha()
{
  $hoy = getdate();
  return "$hoy[mday]/$hoy[mon]/$hoy[year]";
}

//esta funcion escribe una linea de tabla para monitor.php, devuelve el detalle o imprime el PDF directamente

function escribe_linea_detalle($row, $color="#FFFFFF") 
{
  echo "<tr bgcolor=$color ><td><a href=\"inc/detalle.php?id=$row[id]\"target=\"detalleincidencia\">$row[id]</a></td><td>$row[fecha]</td><td>$row[criticidad]</td><td>$row[incidencia]</td><td><a href=inc/class/creapdf.php?id=$row[id]><img src=inc/img/pdf20.png></a></td></tr>";
} 

// Esta funcion muestra el numero de tareas que tiene por realizar un tecnico.

function tareas_tecnico()
{
  $resultado = mysql_query('SELECT id FROM partes WHERE asignado_a=\''.$_SESSION["user"].'\' AND resuelto=0');
  $total = mysql_num_rows($resultado);
  return "$total";
}

// Esta funcion sube ficheros.

function subirfichero($nombre, $directorio)
{
  $ruta_fichero="$_SERVER[DOCUMENT_ROOT]/$directorio";
  if (! is_dir("ruta_fichero"))
  {
    mkdir("$ruta_fichero", 0777);
  }
  $ruta_relativa="$directorio/";
  $name=$_FILES[$nombre]['name'];
  $name=preg_replace("/[^A-z0-9.]/","_",$name);
  copy($_FILES[$nombre]['tmp_name'],"$ruta_fichero/$name");
  unlink($_FILES[$nombre]['tmp_name']);
  $ruta_relativa .= $name;
  return($ruta_relativa);
}

// Esta funcion elimina archivos

function borrafoto($id_parte)
{
  $ruta = mysql_query('SELECT imagen FROM partes WHERE id=\''.$id_parte.'\'');
  $borrarow = mysql_fetch_array($ruta);
  //echo "$_SERVER[DOCUMENT_ROOT]/$borrarow[imagen]";
  unlink("$_SERVER[DOCUMENT_ROOT]/$borrarow[imagen]");
}
function movil()
{
    $hua=$_SERVER['HTTP_USER_AGENT'];
    if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i',strtolower($hua)))$m=true;
    if(strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml')>0||
    ((isset($_SERVER['HTTP_X_WAP_PROFILE'])||isset($_SERVER['HTTP_PROFILE']))))$m=true;
 
    $mua=strtolower(substr($hua,0,4));
    $ma = array('w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki',
    'oper','palm','pana','pant','phil','play','port','prox','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal',
    'smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr','webc','winw','xda','xda-');
    if(in_array($mua,$ma))$m=true;
    if(strpos(strtolower(@$_SERVER['ALL_HTTP']),'OperaMini')>0)$m=true;
    if(strpos(strtolower($hua),'windows')>0&&strpos(strtolower($hua),'IEMobile')<=0)$m=false;
    return $m;
}
// function ventana_error($error)
// {
// 
// }
function mail_registro($correo, $nombre)
{
  $para=$correo;
  $titulo = 'Gracias por registrarte';
  $mensaje = 'Hola '.$nombre.', gracias por registrarte en INRASYS' ;
  $cabeceras = 'From: webmaster@intrasys.dom' . "\r\n" .
    'Reply-To: webmaster@intrasys.dom' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
  mail($para, $titulo, $mensaje, $cabeceras);
}
function mail_creaincidencia($correo, $localizacion, $incidencia)
{
  $para=$correo;
  $titulo = 'Has creado una incidencia con exito';
  $mensaje = 'Hola, tu incidencia ha sido creada con exito, en breve un tecnico la revisara y recibiras un correo cuando este resuelta. Muchas gracias por tu paciencia y comprension' . "\r\n" . "Estos son los datos de tu incidencia: \r\n Ubicacion: $localizacion \r\n Descripcion de la incidencia: \r\n $incidencia";
  $cabeceras = 'From: webmaster@intrasys.dom' . "\r\n" .
    'Reply-To: webmaster@intrasys.dom' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
  mail($para, $titulo, $mensaje, $cabeceras);
}
function mail_asignatarea()
{
  $para=$correo;
  $titulo = '';
  $mensaje = '';
  $cabeceras = 'From: webmaster@intrasys.dom' . "\r\n" .
    'Reply-To: webmaster@intrasys.dom' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
  mail($para, $titulo, $mensaje, $cabeceras);
}
function numero_inci_propias()
{
  $resultado = mysql_query('SELECT id FROM partes WHERE enviado_por=\''.$_SESSION["user"].'\' AND resuelto=0');
  $total = mysql_num_rows($resultado);
  return "$total";
}
function nombre()
{
  $resultado = mysql_query('SELECT nombre FROM usuarios WHERE id=\''.$_SESSION["user"].'\'');
  $nombre = mysql_fetch_array($resultado);
  return "$nombre[nombre]";
}

function PassIncidencia() 
{
$ncaracteres = "8";
$password = "";
$caracteres = "0123456789bcdfghjkmnpqrstvwxyzBCDFGHJKMNPQRSTVWXYZ/!$-.";
$i = 0;
while ($i < $ncaracteres) 
{
  $char = substr($caracteres, mt_rand(0, strlen($caracteres)-1), 1);
  if(!strstr($password,$char)) 
  {
    $password .= $char;
    $i+=1;
  }
}
return $password;
}

function EsPropio($id) //si devuelve 1 quiere decir que SI es el propietario, si devuelve 0 NO es el propietario
{
  $sql = mysql_query('SELECT * FROM partes WHERE id=\''.$id.'\'');
  $row = mysql_fetch_array($sql);
  if ($row['enviado_por'] == $_SESSION['user'])
  {
    return '1';
  }
  else
  {
    return '0';
  }
}
?> 