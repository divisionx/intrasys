<script>
	$(function() {
		$( "#dialogo" ).dialog();
	});
	</script>
<?php
if (! isset($_SESSION['sesion'])) //verificamos si NO existe una sesion abierta
{
  echo "<div id=cajaizq><h2>Login</h2>";
  include "inc/form_acceso.php";
  if ($_GET['logerror']==1)
  {
    echo "<div id=dialogo title='\"Error\"'>";
    echo "<p>Ha habido algun error en el formulario, por favor asegurese de que rellena todos los campos.</p>";
    echo "</div>";
  }
  echo "</div>";

  echo "<div id=busquedad><h2>Registro</h2>";
  $regerror=$_GET['regerror'];
  include "inc/form_registro.php";
  if ($regerror==1)
  {
    echo "<div id=dialogo title='\"Error\"'>";
    echo "<p>Ha habido algun error en el formulario, por favor asegurese de que rellena todos los campos.</p>";
    echo "</div>";
  }
  echo "</div>";
}
else
{
  echo "<div id=busquedai><p>Hola ".nombre().", has iniciado sesion con exito.</p>";
  echo "<p>Usa el menu superior para acceder a las partes del programa.</p><p>Tambien tienes opciones de navegacion en el menu de usuario, justo debajo del menu, pulsando en tu nombre accederas a las opciones de tu cuenta y el numero a su izqda representa las incidencias que tienes publicadas sin resolver.</p>";
  echo "<p>Puedes enviar un bug o una sugerencia a traves del <a href=https://bitbucket.org/divisionx/intrasys/issues/new>issue tracker</a>, tu ayuda es muy agradecida. Si te decides a colaborar te recomendamos registrate en <a href=https://bitbucket.org/divisionx/intrasys/issues/new>BitBucket</a>.</p>";
  if (($_SESSION['niveluser']==2) OR ($_SESSION['niveluser']==1))
  {
    if (tareas_tecnico()==0)
    {
      echo "<p class=aviso>No tienes tareas por realizar.</p>";
    }
    else
    {
      echo "<p class=aviso>Tienes <a href=index.php?op=tareas>". tareas_tecnico() ."</a> tareas por realizar</p>";
    }
  }
  echo "</div>";
  echo "<div id=busquedad><h2>Busqueda</h2>";
  switch ($_REQUEST['buscar'])
  {
    default: //mostramos las opciones de busqueda, pero solo si somos tecnicos o administrador
      if (($_SESSION['niveluser']==2) OR ($_SESSION['niveluser']==1))
      {
	echo '<form action='.$_SERVER['PHP_SELF'].' method="POST">';
	echo '<div class=formulario><label><span>Buscar por ID: </span><input name="buscaid"><input class="boton" type=submit name="buscar" value="busca_id"></label>';
	echo '</div></form>';
	echo '<form action='.$_SERVER['PHP_SELF'].' method="POST">';
	echo '<div class=formulario><label><span>Buscar por descripcion: </span><input name="buscadesc"><input class="boton" type=submit name="buscar" value="busca_desc"></label>';
	echo '</div></form>';
	echo "<h2>buscar historicos</h2>";
	echo '<form action='.$_SERVER['PHP_SELF'].' method="POST">';
	echo '<div class=formulario><label><span>Buscar por solucion: </span><input name="buscahist"><input class="boton" type=submit name="buscar" value="busca_hist"></label>';
	echo '</div></form>';
	echo "<h2></h2><p class=contenido>Puedes usar % como comodin.</p>";
      }
      else
      {
	echo "<p class=aviso>No tienes permisos para realizar busquedas en el sistema, contacta con el administrador del sitio.</p>";
      }
    break;

    case "busca_id":
      echo "<a href=index.php> <-Atras</a>";
      $id= $_POST['buscaid'];
      echo "<center><table class=resultados><tr>";
      echo "<th>ID</th><th>Fecha</th><th>Urgencia</th><th style='width:900px;'>Incidencia</th><th><img src=inc/img/imp20.jpeg alt=IMPRIMIR></th></tr>";

      $resultado = mysql_query('SELECT id, fecha, criticidad, incidencia FROM partes WHERE id=\''.$id.'\'');
      while($row = mysql_fetch_array($resultado)) //rellena el array mientras no se acabe
      {
	  escribe_linea_detalle($row);
      }
      echo "</table></center>";
    break;

    case "busca_desc":
	echo "<a href=index.php> <-Atras</a>";
	$desc=$_POST['buscadesc'];
	$regVistos = 4; // numero de registros mostrados por pagina
	$listarSQL=mysql_query("SELECT * FROM partes WHERE resuelto=0");
	$totalSQL=mysql_num_rows($listarSQL);
	$pagTotal=ceil($totalSQL/$regVistos);
	if (!isset($_GET['pag']))
	{
	  $pagActual=1;
	}
	else
	{
	  $pagActual=$_GET['pag'];
	}
	$pagAnterior=$pagActual-1;
	$pagSiguiente=$pagActual+1;
      
      
	echo "<center><table class=resultados><tr>";
	echo "<th>ID</th><th>Fecha</th><th>Urgencia</th><th style='width:900px;'>Incidencia</th><th><img src=inc/img/imp20.jpeg alt=IMPRIMIR></th></tr>";
      
	$resultado = mysql_query('SELECT id, fecha, criticidad, incidencia FROM partes WHERE resuelto=0 AND incidencia LIKE \'%'.$desc.'%\' LIMIT '.(($pagActual-1)*$regVistos).','.$regVistos.'') or die(mysql_error());
	while($row = mysql_fetch_array($resultado)) //rellena el array mientras no se acabe
	{
	  if($row['criticidad'] <= 3)
	  {
	    escribe_linea_detalle($row, "#00FF00");
	  }
	  if(($row['criticidad'] >= 4) && $row['criticidad'] <= 6)
	  {
	    escribe_linea_detalle($row);
	  }
	  if($row['criticidad'] >= 7)
	  {
	    escribe_linea_detalle($row, "#FFFF00");
	  }
	}
	echo "</table></center>";
      
      
	echo "<div class=paginadorbusca><ul>";
	if ($pagAnterior>0) // si la pagina actual es mayor que 0, mostramos el boton ANTERIOR
	{
	  echo '<li><a href="index.php?buscar=busca_desc&amp;pag='.$pagAnterior.'">Anterior</a></li>'; //boton anterior
	}
	$pgIntervalo = 3; // este es el numero de paginas que aparecen antes y despues de la actual
	$pgMaximo = ($pgIntervalo*2)+1; // Máximo de páginas en el listado
	$pg=$pagActual-$pgIntervalo;$i=0;
	while ($i<$pgMaximo) 
	{
	  if ($pg==$pagActual) 
	  {
	    $strong=array('<strong>','</strong>');
	  }
	  else 
	  {
	    $strong=array('','');
	  }
	  if ($pg>0 and $pg<=$pagTotal) 
	  {
	    echo '<li>'.$strong[0].'<a href="index.php?buscar=busca_desc&amp;pag='.$pg.'">'.$pg.'</a>'.$strong[1].'</li>';
	    $i++;
	  }
	  if ($pg>$pagTotal) 
	  {
	    $i=$pgMaximo;
	  } // Si la página que se va a mostrar se pasa de la cantidad de páginas definidas en $pagTotal se para la generación de elementos de lista
	$pg++;
	}
      
      // Si la página actual no es la última, se muestra el enlace a la página siguiente
      if ($pagSiguiente<=$pagTotal) 
      {
	echo '<li class="siguiente"><a href="index.php?buscar=busca_desc&amp;pag='.$pagSiguiente.'">Siguiente</a></li>';
      }
      // Se finaliza el listado de páginas
      echo '</ul></div>';
    break;


    case "busca_hist":
	echo "<a href=index.php> <-Atras</a>";
	$hist=$_POST['buscahist'];
	$regVistos = 4; // numero de registros mostrados por pagina
	$listarSQL=mysql_query("SELECT * FROM partes WHERE resuelto=0");
	$totalSQL=mysql_num_rows($listarSQL);
	$pagTotal=ceil($totalSQL/$regVistos);
	if (!isset($_GET['pag']))
	{
	  $pagActual=1;
	}
	else
	{
	  $pagActual=$_GET['pag'];
	}
	$pagAnterior=$pagActual-1;
	$pagSiguiente=$pagActual+1;
      
      
	echo "<center><table class=resultados><tr>";
	echo "<th>ID</th><th>Fecha</th><th>Urgencia</th><th style='width:900px;'>Incidencia</th><th><img src=inc/img/imp20.jpeg alt=IMPRIMIR></th></tr>";
      
	$resultado = mysql_query('SELECT id, fecha, criticidad, incidencia FROM partes WHERE resuelto=1 AND solucion LIKE \'%'.$hist.'%\' LIMIT '.(($pagActual-1)*$regVistos).','.$regVistos.'') or die(mysql_error());
	while($row = mysql_fetch_array($resultado)) //rellena el array mientras no se acabe
	{
	  if($row['criticidad'] <= 3)
	  {
	    escribe_linea_detalle($row, "#00FF00");
	  }
	  if(($row['criticidad'] >= 4) && $row['criticidad'] <= 6)
	  {
	    escribe_linea_detalle($row);
	  }
	  if($row['criticidad'] >= 7)
	  {
	    escribe_linea_detalle($row, "#FFFF00");
	  }
	}
	echo "</table></center>";
      
      
	echo "<div class=paginadorbusca><ul>";
	if ($pagAnterior>0) // si la pagina actual es mayor que 0, mostramos el boton ANTERIOR
	{
	  echo '<li><a href="index.php?buscar=busca_hist&amp;pag='.$pagAnterior.'">Anterior</a></li>'; //boton anterior
	}
	$pgIntervalo = 3; // este es el numero de paginas que aparecen antes y despues de la actual
	$pgMaximo = ($pgIntervalo*2)+1; // Máximo de páginas en el listado
	$pg=$pagActual-$pgIntervalo;$i=0;
	while ($i<$pgMaximo) 
	{
	  if ($pg==$pagActual) 
	  {
	    $strong=array('<strong>','</strong>');
	  }
	  else 
	  {
	    $strong=array('','');
	  }
	  if ($pg>0 and $pg<=$pagTotal) 
	  {
	    echo '<li>'.$strong[0].'<a href="index.php?buscar=busca_hist&amp;pag='.$pg.'">'.$pg.'</a>'.$strong[1].'</li>';
	    $i++;
	  }
	  if ($pg>$pagTotal) 
	  {
	    $i=$pgMaximo;
	  } // Si la página que se va a mostrar se pasa de la cantidad de páginas definidas en $pagTotal se para la generación de elementos de lista
	$pg++;
	}
      
      if ($pagSiguiente<=$pagTotal) 
      {
	echo '<li class="siguiente"><a href="index.php?buscar=busca_hist&amp;pag='.$pagSiguiente.'">Siguiente</a></li>';
      }
      // Se finaliza el listado de páginas
      echo '</ul></div>';
    break;
  }
}
?>