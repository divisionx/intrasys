<?php
session_start();
ob_start();
include "inc/config.php";
include "inc/funciones.php";
//include "inc/cabecera.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<?php
if (movil()==true) //si el usuario es un movil.
{
  echo "<link rel=stylesheet type=text/css href=inc/css/estilomovil.css>";
  echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />";
}
else
{ //si no es un navegador movil
?>
<link rel="stylesheet" type="text/css" href="inc/css/estilo.css">
<link type="text/css" href="inc/css/jquery/css/smoothness/jquery-ui-1.8.17.custom.css" rel="Stylesheet">	
<script type="text/javascript" src="inc/css/jquery/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="inc/css/jquery/js/jquery-ui-1.8.17.custom.min.js"></script>
<?php
} // Esta llave finaliza el else condicion de si es un navegador movil.
?>
<title>INTRASYS</title>
</head>
<body>

<div id=contenido>
<?php
include "inc/menu.php";
?>
<h1>al servicio de sus necesidades</h1>
<?php
if (! isset($_SESSION['sesion'])) //verificamos si NO existe una sesion abierta
{
  switch ($_REQUEST['op'])
  {
    default: //mostramos un menu, registro o login
      include "inc/inicio.php";
    break;
    case "registrar":  //formulario de registro
      include "inc/form_registro.php";
    break;
    case "registro": //mecanismo de registro
      include "inc/registro.php";
    break;
    case "acceder":  //formulario de login
      include "inc/form_acceso.php";
    break;
    case "acceso": //mecanismo de acceso
      include "inc/acceso.php";
    break;
  }
}
else
{
echo "<h4>Bienvenido <a href=$_SERVER[PHP_SELF]?op=panusuario>".nombre()."</a> <a href=$_SERVER[PHP_SELF]?op=moninc&usuario=user>(". numero_inci_propias().")</a> </h4>";

    switch ($_REQUEST['op'])
    {
      default:
	include "inc/inicio.php";
	echo "</div>";
      break;
      case "logout": //cierre de sesion
	session_destroy();
	echo "Has cerrado sesion con exito. Vuelve pronto.";
	header("location:index.php");
      break;
      case "crearinc": //formulario para crear una incidencia
	include "inc/form_incidencia.php";
      break;
      case "insertarinc": //añadimos la incidencia a la BD
	include "inc/incidencia.php";
      break;
      case "moninc": //monitor de incidencias
	include "inc/monitor.php";
      break;
      case "tareas": //taresas del usuario
	include "inc/tareas.php";
      break;
      case "historico": //tareas resueltas y archivadas
	include "inc/historico.php";
      break;
      case "panusuario": //panel de modificacion de usuario
	include "inc/usuario.php";
      break;
      case "admin":
	header("location:admin/index.php");	
      break;
    }
}
?>
</div>

<div id=footer>
<p><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png"></a><a href="http://sourceforge.net/projects/intrasys/"><img src="http://geek.net/files/4712/7378/2653/sf_over.png" alt="SourceForge"></a><a href="http://php.net"><img class=der alt="Powered By PHP" style="border-width:0" src="inc/img/php-power-white.png"></a><a href="http://jigsaw.w3.org/css-validator/check/referer"><img class=der style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="¡CSS Válido!">
    </a></p>
</div>
</body>
</html>