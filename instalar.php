  <?php
ob_start();
include "inc/config.php";
include "inc/funciones.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="inc/css/estilo.css">
<link type="text/css" href="inc/css/jquery/css/smoothness/jquery-ui-1.8.17.custom.css" rel="Stylesheet">	
<script type="text/javascript" src="inc/css/jquery/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="inc/css/jquery/js/jquery-ui-1.8.17.custom.min.js"></script>

<title>INTRASYS</title>
</head>
<body>

<div id=contenido>
<h1>Instalador de INTRASYS</h1>
<?php
if (movil()==true) //si el usuario es un movil.
{
  echo "<p class=contenido>Este script debe instalarse desde un navegador web estandar";
}
else
{ //si no es un navegador movil
  switch($_REQUEST['op'])
  {
    default:
      echo "<p class=contenido>Bienvenido al instalador de INTRASYS, a continuacion debera introducir los datos que se requieren</p>";
      echo "<p class=contenido>Por favor, verifique que todos los datos sean los correctos antes de pulsar el boton \"instalar\", de no ser asi el programa no podra finalizar la instalacion correctamente y el sistema puede quedar corrupto, siendo asi necesario reinstalar INTRASYS</p>";

      echo '<form action='.$_SERVER['PHP_SELF'].' method="POST" enctype="multipart/form-data">';
      echo '<div class="formulario"><fieldset class=i><h2>Base de datos</h2><label><span>Host de la BD: </span><input name="host" size="30"/></label>';
      echo '<label><span>Nombre de usuario de la BD: </span><input name="userBD" size="30"/></label>';
      echo '<label><span>Contraseña de la BD: </span><input name="passBD" size="30"/></label>';
      echo '<label><span>Nombre de la BD: </span><input name="bd" size="30"/></label></fieldset>';
  
      echo '<fieldset class=i><h2>Cuenta de administracion</h2><label><span>Nombre: </span><input name="nombre" size="30"/></label>';
      echo '<label><span>Apellidos: </span><input name="apellidos" size="30"/></label>';
      echo '<label><span>Nombre de usuario: </span><input name="user" size="30"/></label>';
      echo '<label><span>Contraseña: </span><input name="pass" size="30"/></label>';
      echo '<label><span>Email: </span><input name="email" size="30"/></label>';
      echo '<input type=submit name="op" value="instalar"></fieldset>';
      echo '</div></form>';
    break;

    case "instalar":
      $mysql_host = $_REQUEST['host'];
      $mysql_user = $_REQUEST['userBD'];
      $mysql_password = $_REQUEST['passBD'];

      $conexion=mysql_connect($mysql_host, $mysql_user, $mysql_password);

      // Creamos Base de dtos principal
      mysql_query("CREATE DATABASE IF NOT EXISTS $_REQUEST[bd]");
      mysql_select_db ("$_REQUEST[bd]");

      // Y sus tablas correspondientes
      mysql_query("CREATE TABLE IF NOT EXISTS `comentarios` 
      (
	`com_id` int(7) unsigned zerofill NOT NULL AUTO_INCREMENT,
	`inc_id` int(6) NOT NULL,
	`usr_id` int(6) NOT NULL,
	`fecha` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
	`comentario` text COLLATE utf8_spanish_ci NOT NULL,
	PRIMARY KEY (`com_id`)
      ) 
      ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ;");
      
      mysql_query("CREATE TABLE IF NOT EXISTS `partes` 
      (
	`id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
	`enviado_por` int(4) NOT NULL,
	`asignado_a` int(4) NOT NULL DEFAULT '0',
	`fecha` varchar(10) NOT NULL,
	`localizacion` varchar(50) NOT NULL,
	`marca` varchar(10) DEFAULT NULL,
	`modelo` varchar(20) DEFAULT NULL,
	`incidencia` text NOT NULL,
	`criticidad` int(2) NOT NULL,
	`resuelto` tinyint(1) NOT NULL DEFAULT '0',
	`solucion` text,
	`revisiones` int(2) NOT NULL DEFAULT '0',
	`imagen` varchar(100) NOT NULL,
	PRIMARY KEY (`id`)
      ) 
      ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;");

      mysql_query("CREATE TABLE IF NOT EXISTS `usuarios` 
      (
	`id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
	`user` varchar(15) NOT NULL,
	`pass` varchar(32) NOT NULL,
	`nombre` varchar(15) NOT NULL,
	`apellidos` varchar(30) NOT NULL,
	`email` text NOT NULL,
	`tipo` int(1) NOT NULL DEFAULT '3',
	PRIMARY KEY (`id`)
      ) 
      ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;");

      mysql_query("INSERT INTO `usuarios` (`id`, `user`, `pass`, `nombre`, `apellidos`, `email`, `tipo`) VALUES
      (0001, '$_REQUEST[user]', '".md5($_REQUEST['pass'])."', '$_REQUEST[nombre]', '$_REQUEST[apellidos]', '$_REQUEST[email]', 1);");

      echo "<p class=contenido>La instalacion se ha realizado con exito, no se olvide de eliminar este fichero \"instalar.php\"</p>";
      echo "<p class=contenido>Puede empezar a disfrutar de su sistema desde ya. Si tiene dudas sobre su uso puede referirse al manual de usuario, o ponerse en contacto con el proveedor de su licencia INTRASYS.</p>";
    break;
  }
}
?>
</div>

</body>
</html>